
/*
* Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
* SPDX-License-Identifier: BSD-3-Clause-Clear
*/


#ifndef BATTERYSERVER_HPP
#define BATTERYSERVER_HPP

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <ctype.h>
#include <dbus/dbus.h>
#include <vector>
#include <map>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

class BatteryServer {

public:
    BatteryServer();
    void wait_for_client();

private:
    std::string powerSupplySysfsPath; //TODO I want this to be const
    std::string logFilePath; //TODO I want this to be const
    std::vector<std::string> powerSupplyNodes;
    void print_dbus_error (char *str);

    void init_dbus();
    void parse_config_file(std::string configFilePath);


    std::string extract_string_from_config(const std::string& configFile, const std::string& variableName);


    std::vector<std::string> extract_nodes_from_config(const std::string& configFile, const std::string& variableName);
    void query_sysfs();
    std::string convert_map_to_char();
    DBusConnection *mConn;
    DBusError dbus_error;
    std::map<std::string, std::string> mBatteryStatsMap;
};

#endif // BATTERYSERVER_HPP
