/*
* Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
* SPDX-License-Identifier: BSD-3-Clause-Clear
*/

#include "../inc/battery_server.hpp"


const char *const SERVER_BUS_NAME = "in.battery_server";
const char *const INTERFACE_NAME = "in.battery_server.system";
const char *const OBJECT_PATH_NAME = "/in/battery_system/server";
const char *const METHOD_NAME = "get_battery_stats";


void BatteryServer::init_dbus(){
    int ret;
    dbus_error_init(&dbus_error);
    mConn = dbus_bus_get(DBUS_BUS_SYSTEM, &dbus_error);

    if (dbus_error_is_set(&dbus_error))
        print_dbus_error("dbus_bus_get");

    if (!mConn)
        exit (1);

    // Get a well known name
    ret = dbus_bus_request_name(mConn, SERVER_BUS_NAME, DBUS_NAME_FLAG_REPLACE_EXISTING, &dbus_error);

    if (dbus_error_is_set(&dbus_error))
        print_dbus_error("dbus_bus_get");

    if (ret != DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER) {
        fprintf (stderr, "Dbus: not primary owner, ret = %d\n", ret);
        exit (1);
    }
}

std::string BatteryServer::extract_string_from_config(const std::string& configFile, const std::string& variableName){
    std::ifstream file(configFile);
    std::string line;
    std::string searchString = variableName + " = \"";
    while (std::getline(file, line)) {
        size_t pos = line.find(searchString);
        if (pos != std::string::npos) {
            size_t start = pos + searchString.length();
            size_t end = line.find('"', start);
            if (end != std::string::npos) {
                file.close();
                return line.substr(start, end - start);
            }
        }
    }
    file.close();
    return "";
}

std::vector<std::string> BatteryServer::extract_nodes_from_config(const std::string& configFile, const std::string& variableName) {
    std::ifstream file(configFile);
    std::string line;
    std::vector<std::string> nodes;
    bool insideTargetVariable = false;

    while (std::getline(file, line)) {
        // Check if the line contains the target variable
        if (line.find(variableName) != std::string::npos) {
            insideTargetVariable = true;
        } else if (insideTargetVariable) {
            // If inside the target variable, add the line to the nodes vector
            if (!line.empty() && line != variableName + ":") {
                nodes.push_back(line);
            }
        }
    }

    file.close();
    return nodes;
}


void BatteryServer::parse_config_file(std::string configFilePath){

    logFilePath = extract_string_from_config(configFilePath, "LOGFILE");
    powerSupplySysfsPath = extract_string_from_config(configFilePath, "POWER_SUPPLY_SYSFS_PATH");
    powerSupplyNodes = extract_nodes_from_config(configFilePath, "POWER_SUPPLY_NODES");


    std::ifstream sysNodeFile;
    for (auto it = powerSupplyNodes.begin(); it != powerSupplyNodes.end();) {
        auto path = *it;
        std::string fullPath = powerSupplySysfsPath + path;
        sysNodeFile.open(fullPath);
        if (!sysNodeFile.is_open()) {
            // If file does not exist, remove it from the vector
            it = powerSupplyNodes.erase(it);
        }
        else {
            sysNodeFile.close();
            ++it;
        }
    }
}


BatteryServer::BatteryServer(){
    init_dbus();
    parse_config_file("/etc/battery-config.txt");
    
}

void BatteryServer::print_dbus_error (char *str) {
    fprintf (stderr, "%s: %s\n", str, dbus_error.message);
    dbus_error_free (&dbus_error);
}

//should be name connect to client
void BatteryServer::wait_for_client(){
    // Handle request from clients
    while (1) {
        // Block for msg from client
        if (!dbus_connection_read_write_dispatch(mConn, -1)) {
            fprintf (stderr, "Not connected now.\n");
            exit (1);
        }

        //start of get message
        DBusMessage *message;

        if ((message = dbus_connection_pop_message(mConn)) == NULL) {
            fprintf (stderr, "Did not get message\n");
            continue;
        }

        if (dbus_message_is_method_call(message, INTERFACE_NAME, METHOD_NAME)) {
            std::cout << "Got method call" << std::endl;
            query_sysfs();
            auto data = convert_map_to_char();
            DBusMessage *reply;

            if ((reply = dbus_message_new_method_return(message)) == NULL) {
                fprintf (stderr, "Error in dbus_message_new_method_return\n");
                exit (1);
            }

            DBusMessageIter iter;
            dbus_message_iter_init_append(reply, &iter);
            // const char *ptr = data.c_str();
            char answer[64*64];
            snprintf(answer, sizeof(answer), "%s", data.c_str());
            char* ptr = answer;
            if (!dbus_message_iter_append_basic(&iter, DBUS_TYPE_STRING, &ptr)) {
                fprintf (stderr, "Error in dbus_message_iter_append_basic\n");
                exit (1);
            }

            if (!dbus_connection_send(mConn, reply, NULL)) {
                fprintf (stderr, "Error in dbus_connection_send\n");
                exit (1);
            }

            dbus_connection_flush(mConn);

            dbus_message_unref(reply);
        }
    }
}

std::string BatteryServer::convert_map_to_char(){
    std::string dataLine = "";
    for(auto &data: mBatteryStatsMap){
        std::string mapline = "";
        mapline = data.first + ":" + data.second;
        dataLine += mapline;
        dataLine += "\n";
    }
    return dataLine;
}

void BatteryServer::query_sysfs(){
    //query the sysfs for battery information
    //place it in the map which could be a shared pointer to avoid copying

    std::ifstream batteryFile;
    for(size_t i = 0; i < powerSupplyNodes.size(); i++) {
        auto path = powerSupplyNodes[i];
        std::string fullPath = powerSupplySysfsPath + path;
        batteryFile.open(fullPath);
        if(batteryFile.is_open()) {
            std::string line;
            std::getline(batteryFile, line);

            if(line.empty()) {
                line = "EMPTY";
            }
            mBatteryStatsMap[path] = line;
            batteryFile.close();
        }
    }
}


int main (int argc, char **argv)
{
    std::cout << "Battery Server is running" << std::endl;
    BatteryServer bs;
    bs.wait_for_client();
    return 0;
}
