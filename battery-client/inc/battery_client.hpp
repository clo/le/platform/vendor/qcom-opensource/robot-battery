/*
* Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
* SPDX-License-Identifier: BSD-3-Clause-Clear
*/

#ifndef BATTERYCLIENT_HPP
#define BATTERYCLIENT_HPP

#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <ctype.h>
#include <dbus/dbus.h>
#include <map>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

class BatteryClient {
public:
    BatteryClient();
    char* get_battery_stats_API_alloc();
    void get_battery_stats_user_alloc(char* usrptr, size_t size);
    
private:
    void init_dbus_connection();
    char* get_server_msg();
    void print_dbus_error (char *str);
    DBusConnection *mConn;
    DBusError dbus_error;
};

#endif // BATTERYCLIENT_HPP

