/*
* Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
* SPDX-License-Identifier: BSD-3-Clause-Clear
*/

#include "../inc/battery_client.hpp"



const char *const SERVER_BUS_NAME = "in.battery_server"; 
const char *const INTERFACE_NAME = "in.battery_server.system"; 
const char *const SERVER_OBJECT_PATH_NAME = "/in/battery_system/server"; 
const char *const METHOD_NAME = "get_battery_stats"; 

BatteryClient::BatteryClient()
{
    init_dbus_connection();
}

void BatteryClient::init_dbus_connection()
{
    dbus_error_init (&dbus_error);
    mConn = dbus_bus_get (DBUS_BUS_SYSTEM, &dbus_error);

    if (dbus_error_is_set (&dbus_error))
        print_dbus_error ("dbus_bus_get");

    if (!mConn) 
        exit (1);
}

char* BatteryClient::get_server_msg()
{

    //Constructs a new message to invoke a method on a remote object
    DBusMessage *request;
    DBusError dbus_error1;
    dbus_error_init(&dbus_error1);
    if ((request = dbus_message_new_method_call (SERVER_BUS_NAME, SERVER_OBJECT_PATH_NAME, 
                    INTERFACE_NAME, METHOD_NAME)) == NULL) {
        fprintf (stderr, "Error in dbus_message_new_method_call\n");
        exit (1);
    }

    
    //Queues a message to send, as with dbus_connection_send()
    DBusPendingCall *pending_return;
    if (!dbus_connection_send_with_reply (mConn, request, &pending_return, -1)) {
        fprintf (stderr, "Error in dbus_connection_send_with_reply\n");
        exit (1);
    }
    if (pending_return == NULL) {
        fprintf (stderr, "pending return is NULL");
        exit (1);
    }

    dbus_connection_flush (mConn);
    dbus_message_unref (request);
    dbus_pending_call_block (pending_return);

    //Gets the reply
    DBusMessage *reply;
    if ((reply = dbus_pending_call_steal_reply (pending_return)) == NULL) {
        fprintf (stderr, "Error in dbus_pending_call_steal_reply");
        exit (1);
    }

    dbus_pending_call_unref	(pending_return);


    char * s; //TODO:: does this need a better name?
    if (!dbus_message_get_args (reply, &dbus_error1, DBUS_TYPE_STRING, &s, DBUS_TYPE_INVALID)) {
        fprintf (stderr, "Did not get arguments in reply\n");
        exit (1);
    }
    dbus_message_unref (reply);

    return s;
}


/***********************************************************************************************************
 * The battery client allocates the data. User must remember to free data otherwise will face memory leaks *
************************************************************************************************************/
char* BatteryClient::get_battery_stats_API_alloc()
{
    DBusError dbus_error2;
    dbus_error_init(&dbus_error2);
    dbus_bus_add_match(mConn, "type='message',interface='in.battery_server.system'", &dbus_error2);
    
    auto msg = get_server_msg();

    char* retmsg = strdup(msg);
    DBusError dbus_error3;
    dbus_error_init(&dbus_error3);
    if (dbus_bus_release_name (mConn, SERVER_BUS_NAME, &dbus_error3) == -1) {
        fprintf (stderr, "Error in dbus_bus_release_name\n");
        exit (1);
    }

    return retmsg;
}


/*************************************************************************************************
 * The user allocates the data. User must remember to free data otherwise will face memory leaks *
**************************************************************************************************/
void BatteryClient::get_battery_stats_user_alloc(char* usrptr, size_t size)
{
    int ret;
    
    // Get a well known name
    while (1) {
        ret = dbus_bus_request_name (mConn, SERVER_BUS_NAME, 0, &dbus_error);

        if (ret == DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER) 
            break;

        if (ret == DBUS_REQUEST_NAME_REPLY_IN_QUEUE) {
            fprintf (stderr, "Waiting for the bus ... \n");
            sleep (1);
            continue;
        }
        if (dbus_error_is_set (&dbus_error))
            print_dbus_error ("dbus_bus_get");
    }
    
    auto msg = get_server_msg();

    //copy data into user.. check if data to copy is larger than user intended.
    size_t msglen = strlen(msg);
    size_t tocopy = std::min(msglen, size);
    
    std::vector<char> destination_vec(msg, msg + tocopy);
    // Ensure null termination
    destination_vec.push_back('\0');

    // Get a pointer to the data
    usrptr = destination_vec.data();

    if (dbus_bus_release_name (mConn, SERVER_BUS_NAME, &dbus_error) == -1) {
        fprintf (stderr, "Error in dbus_bus_release_name\n");
        exit (1);
    }
}

void BatteryClient::print_dbus_error (char *str)
{
    fprintf(stderr, "%s: %s\n", str, dbus_error.message);
    dbus_error_free(&dbus_error);
}
