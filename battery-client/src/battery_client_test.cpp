/*
* Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
* SPDX-License-Identifier: BSD-3-Clause-Clear
*/

#include "../inc/battery_client.hpp"


void userAlloc(BatteryClient* bc)
{
    char input;
    std::cout << "Type 's' to get battery stats: " << std::endl;
    while (1) {
        char* batData = new char[1000];
        std::cin >> input;
        if (input != 's') {
            break;
        }
        bc->get_battery_stats_user_alloc(batData, 1000);
        std::cout << "Battery Stats: " << batData << std::endl;
        std::cout << std::endl;
        //!!!!!USER MUST FREE ALLOC DATA!!!!!
        delete[] batData;
        std::cout << "Type 's' to get battery stats: " << std::endl;
    }
}

void apiAlloc(BatteryClient* bc)
{
    char input;
    std::cout << "Type 's' to get battery stats: " << std::endl;
    while (1) {
        std::cin >> input;
        if (input != 's') {
            break;
        }
        auto dat = bc->get_battery_stats_API_alloc(); ///strdup
        std::cout << "Battery Stats:: " << dat << std::endl;
        std::cout << std::endl;
        //!!!!!USER MUST FREE ALLOC DATA!!!!!
        free(dat);
        std::cout << "Type 's' to get battery stats:: " << std::endl;
    }
}

int main (int argc, char **argv)
{
    BatteryClient bc;
    apiAlloc(&bc);
    return 0;
}