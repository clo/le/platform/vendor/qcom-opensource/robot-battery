/*
* Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
* SPDX-License-Identifier: BSD-3-Clause-Clear
*/

#ifndef BATTERYNODE_HPP
#define BATTERYNODE_HPP

#include "rclcpp/rclcpp.hpp"
#include <string>
#include <sensor_msgs/msg/battery_state.hpp>
#include "battery_client.hpp"
#include <map>

namespace qrb_ros_battery
{
    class BatteryStatsReader : public rclcpp::Node {
    public:
        BatteryStatsReader();
    private:
        void timer_callback();
        sensor_msgs::msg::BatteryState pack_battery_state_msg(std::map<std::string, std::string>& data);
        std::map<std::string, std::string> string_to_map(char* str);
        rclcpp::TimerBase::SharedPtr timer_;
        rclcpp::Publisher<sensor_msgs::msg::BatteryState>::SharedPtr publisher_;
        float safestof(std::string str);
    };
}

#endif //BATTERYNODE_HPP

