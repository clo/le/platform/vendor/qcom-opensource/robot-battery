/*
* Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
* SPDX-License-Identifier: BSD-3-Clause-Clear
*/


#include <chrono>
#include <memory>
#include <iostream>
#include <string>
#include <fstream>

#include <rclcpp/rclcpp.hpp>

#include "qrb_ros_battery/battery_node.hpp"

using namespace std::chrono_literals;
using namespace qrb_ros_battery;

// This example creates a subclass of Node and uses std::bind() to register a
// member function as a callback from the timer. */

BatteryStatsReader::BatteryStatsReader() : Node("battery_stats_publisher") 
{
    RCLCPP_INFO(this->get_logger(), "Started Battery Node");
	publisher_ = this->create_publisher<sensor_msgs::msg::BatteryState>("battery_stats", 10);
    timer_ = this->create_wall_timer(
      2000ms, std::bind(&BatteryStatsReader::timer_callback, this));
}

float BatteryStatsReader::safestof(std::string str)
{
    if(str=="EMPTY") return -1;
    return std::stof(str);
}

sensor_msgs::msg::BatteryState BatteryStatsReader::pack_battery_state_msg(std::map<std::string, std::string>& data)
{

    sensor_msgs::msg::BatteryState batteryStateMsg;
 
    batteryStateMsg.header.stamp = this->now();
    batteryStateMsg.header.frame_id = "battery_stats";

    batteryStateMsg.voltage = safestof(data["voltage_now"]); //0
    batteryStateMsg.temperature = safestof(data["temp"]); //0
    batteryStateMsg.current = safestof(data["current_now"]); //0
    batteryStateMsg.charge = safestof(data["charge_counter"]); //0
    batteryStateMsg.capacity = safestof(data["capacity"]); //50
    batteryStateMsg.design_capacity = safestof(data["charge_full_design"]); //0
    batteryStateMsg.percentage = safestof(data["capacity"]); //50

    //setting Power Supply Status constants
    if(data["status"]=="Charging") batteryStateMsg.power_supply_status = 1;
    else if(data["status"]=="Discharging") batteryStateMsg.power_supply_status = 2;
    else if(data["status"]=="Not charging") batteryStateMsg.power_supply_status = 3;
    else if(data["status"]=="Full") batteryStateMsg.power_supply_status = 4;
    else batteryStateMsg.power_supply_status = 0;

    //setting Power Supply Health constants
    if(data["health"]=="Unknown") batteryStateMsg.power_supply_health = 0;
    else if(data["health"]=="Good") batteryStateMsg.power_supply_health = 1;
    else if(data["health"]=="Overheat") batteryStateMsg.power_supply_health = 2;
    else if(data["health"]=="Dead") batteryStateMsg.power_supply_health = 3;
    else if(data["health"]=="Over voltage") batteryStateMsg.power_supply_health = 4;
    else if(data["health"]=="Unspecified failure") batteryStateMsg.power_supply_health = 5;
    else if(data["health"]=="Cold") batteryStateMsg.power_supply_health = 6;
    else if(data["health"]=="Watchdog timer expire") batteryStateMsg.power_supply_health = 7;
    else if(data["health"]=="Safety timer expire") batteryStateMsg.power_supply_health = 8;

    //setting Power Supply Technology constants
    if(data["technology"]=="Unknown") batteryStateMsg.power_supply_technology = 0;
    else if(data["technology"]=="NiMH") batteryStateMsg.power_supply_technology = 1;
    else if(data["technology"]=="Li-ion") batteryStateMsg.power_supply_technology = 2;
    else if(data["technology"]=="Li-poly") batteryStateMsg.power_supply_technology = 3;
    else if(data["technology"]=="LiFe") batteryStateMsg.power_supply_technology = 4;
    else if(data["technology"]=="NiCd") batteryStateMsg.power_supply_technology = 5;
    else if(data["technology"]=="LiMn") batteryStateMsg.power_supply_technology = 6;


    batteryStateMsg.present = std::stoi(data["present"]); //1

    batteryStateMsg.location = "battery_stats";
    batteryStateMsg.serial_number = "battery_stats";
    batteryStateMsg.cell_voltage.push_back(safestof(data["voltage_now"]));//0
    batteryStateMsg.cell_temperature.push_back(safestof(data["temp"]));//0

    return batteryStateMsg;
}

std::map<std::string, std::string> BatteryStatsReader::string_to_map(char* str)
{
    std::map<std::string, std::string> result;
    std::istringstream iss(str);

    std::string line;
    while (std::getline(iss, line)) {
        size_t delimiterPos = line.find(':');
        if (delimiterPos != std::string::npos) {
            std::string key = line.substr(0, delimiterPos);
            std::string value = line.substr(delimiterPos + 1);
            result[key] = value;
        }
    }

    return result;
}

void BatteryStatsReader::timer_callback()
{
    std::cout << "begin timer callback" << std::endl;
    BatteryClient bc;
    std::cout << "initialized bc" << std::endl;
    auto data = bc.get_battery_stats_API_alloc();//Action line
    std::cout << data << std::endl;

    auto bstatmap = string_to_map(data);

    auto msg = pack_battery_state_msg(bstatmap);
    publisher_->publish(msg);

    std::cout << "Published Battery Stats" << std::endl;
}

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<BatteryStatsReader>());
  rclcpp::shutdown();
  return 0;
}
